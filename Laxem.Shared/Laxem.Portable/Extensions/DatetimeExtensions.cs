﻿using System;

namespace Laxem.Portable.Extensions
{
    public static class DatetimeExtensions
    {

        public static DateTimeOffset ToKeDatetime(this DateTimeOffset date)
        {
            return date.AddHours(3);
        }

        public static string GetSalutation(this DateTimeOffset currentDateTime)
        {
            var hour = currentDateTime.Hour;
            string result = string.Empty;
            if (hour >= 1 && hour <= 12)
            {
                result = "Good Morning";
            }
            else if (hour >= 12 && hour <= 16)
            {
                result = "Good Afternoon";
            }
            else if (hour >= 16 && hour <= 21)
            {
                result = "Good Evening";
            }
            //else if (hour >= 21 && hour <= 24)
            //{
            //    result = "Good Night";
            //}

            return result;
        }
    }
}
