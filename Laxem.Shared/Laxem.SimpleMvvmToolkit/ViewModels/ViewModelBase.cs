﻿using System.ComponentModel;

namespace Laxem.SimpleMvvmToolkit.ViewModel
{
    public abstract class ViewModelBase<TModel> where TModel : class, INotifyPropertyChanged
    {

        private TModel _model;
        public TModel Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value;
                NotifyPropertyChanged(nameof(Model));
            }

        }

        public abstract void OnViewReady();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            OnPropertyChanged(propertyName);
        }
    }
}
