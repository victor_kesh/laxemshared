﻿namespace Laxem.Portable.Enums
{
    public enum ApplicationIds
    {
        HorizonApp =1,
        HorizonRestService = 2,
        HorizonWeb = 3,
        PickMeAppAndroid = 4,
        PickMeAppRestService = 5,
    }
}
