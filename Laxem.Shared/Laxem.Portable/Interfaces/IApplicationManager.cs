﻿using Laxem.Portable.Utilities;

namespace Laxem.Portable.Interfaces
{
    public interface IApplicationManager
    {
        ISecurityManager SecurityManager { get;  }
        int ApplicationId { get; }
        string ApplicationVersion { get; }
        string LoginHours { get; }
        string HelloMessage { get; }
        int AllowedLoginTrials { get; }
        string ApplicationBaseUrl { get; }
        ICommonOperationStatus SendRestRequest(object obj, string address, RestSharp.Method method);
    }
}
