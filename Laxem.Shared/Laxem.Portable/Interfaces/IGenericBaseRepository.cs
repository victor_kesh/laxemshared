﻿using Laxem.Portable.Helpers;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Laxem.Portable.Interfaces
{
    public interface IGenericBaseRepository<T> where T : class
    {
        OperationStatus Add(T Entity);
        OperationStatus AddAndGetId(T Entity);
        List<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        OperationStatus Remove(T entity);
        OperationStatus Update(T entity);
        OperationStatus AddMany(List<T> items);
    }
}