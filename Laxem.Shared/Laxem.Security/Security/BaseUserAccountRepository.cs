﻿using Laxem.Portable.Helpers;
using Laxem.Security.Repositories;
using Laxem.Security.Security.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Laxem.Security.Security
{
    public abstract class BaseUserAccountRepository<T> : BaseRepository<T> where T : class
    {
        public BaseUserAccountRepository(DbContext context) : base(context)
        {
        }

        public OperationStatus AddUserAccount(ICommonUserAccount userAccount)
        {
            return Add((T)userAccount);
        }

        public virtual OperationStatus UpdateUserAccount(ICommonUserAccount userAccount)
        {
            return Update((T)userAccount);
        }
        public IEnumerable<ICommonUserAccount> GetUserAccount(Func<T, bool> where,
            params Expression<Func<T, object>>[] navigationProperties)
        {
            return GetList(where, navigationProperties) as List<ICommonUserAccount>;
        }


    }
}
