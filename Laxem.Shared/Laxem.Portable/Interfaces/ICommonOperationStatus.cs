﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laxem.Portable.Interfaces
{
   public interface ICommonOperationStatus
    {
         string ErrorMessage { get; set; }
         string Status { get; set; }
         object Result { get; set; }
    }
}
