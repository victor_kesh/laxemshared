﻿using System;

namespace Laxem.PubSub
{
    public abstract class PubSubBase 
    {
        #region Messaging Helpers

        // Proxy for communication with the MessageBus
        private MessageBusProxy messageBusHelper = new MessageBusProxy();

        /// <summary>
        /// Register callback using a string token, which is usually defined as a constant.
        /// </summary>
        /// <para>
        /// There is no need to unregister because receivers are allowed to be garbage collected.
        /// </para>
        /// <param name="token">String identifying a message token</param>
        /// <param name="callback">Method to invoke when notified</param>
        protected void RegisterToReceiveMessages(string token, EventHandler<NotificationEventArgs> callback)
        {
            // Register callback with MessageBusHelper and MessageBus
            this.messageBusHelper.Register(token, callback);
            MessageBus.Default.Register(token, this.messageBusHelper);
        }

        /// <summary>
        /// Register callback using string token and notification with TOutgoing data.
        /// </summary>
        /// <para>
        /// There is no need to unregister because receivers are allowed to be garbage collected.
        /// </para>
        /// <typeparam name="TOutgoing">Type used by notifier to send data</typeparam>
        /// <param name="token">String identifying a message token</param>
        /// <param name="callback">Method to invoke when notified</param>
        protected void RegisterToReceiveMessages<TOutgoing>(string token,
            EventHandler<NotificationEventArgs<TOutgoing>> callback)
        {
            // Register callback with MessageBusHelper and MessageBus
            this.messageBusHelper.Register(token, callback);
            MessageBus.Default.Register(token, this.messageBusHelper);
        }

        /// <summary>
        /// Register callback using string token and notification with TOutgoing data
        /// and the subscriber's callback with TIncoming data.
        /// </summary>
        /// <para>
        /// There is no need to unregister because receivers are allowed to be garbage collected.
        /// </para>
        /// <typeparam name="TOutgoing">Type used by notifier to send data</typeparam>
        /// <typeparam name="TIncoming">Type sent by subscriber to send data back to notifier</typeparam>
        /// <param name="token">String identifying a message token</param>
        /// <param name="callback">Method to invoke when notified</param>
        protected void RegisterToReceiveMessages<TOutgoing, TIncoming>(string token,
            EventHandler<NotificationEventArgs<TOutgoing, TIncoming>> callback)
        {
            // Register callback with MessageBusHelper and MessageBus
            this.messageBusHelper.Register(token, callback);
            MessageBus.Default.Register(token, this.messageBusHelper);
        }

        /// <summary>
        /// Unregister callback using a string token, which is usually defined as a constant.
        /// </summary>
        /// <para>
        /// This is optional because registered receivers are allowed to be garbage collected.
        /// </para>
        /// <param name="token">String identifying a message token</param>
        /// <param name="callback">Method to invoke when notified</param>
        protected void UnregisterToReceiveMessages(string token, EventHandler<NotificationEventArgs> callback)
        {
            // Unregister callback with MessageBusHelper and MessageBus
            this.messageBusHelper.Unregister(token, callback);
            MessageBus.Default.Unregister(token, this.messageBusHelper);
        }

        /// <summary>
        /// Unregister callback using string token and notification with TOutgoing data.
        /// </summary>
        /// <para>
        /// This is optional because registered receivers are allowed to be garbage collected.
        /// </para>
        /// <typeparam name="TOutgoing">Type used by notifier to send data</typeparam>
        /// <param name="token">String identifying a message token</param>
        /// <param name="callback">Method to invoke when notified</param>
        protected void UnregisterToReceiveMessages<TOutgoing>(string token,
            EventHandler<NotificationEventArgs<TOutgoing>> callback)
        {
            // Unregister callback with MessageBusHelper and MessageBus
            this.messageBusHelper.Unregister(token, callback);
            MessageBus.Default.Unregister(token, this.messageBusHelper);
        }

        /// <summary>
        /// Unregister callback using string token and notification with TOutgoing data
        /// and the subscriber's callback with TIncoming data.
        /// </summary>
        /// <para>
        /// This is optional because registered receivers are allowed to be garbage collected.
        /// </para>
        /// <typeparam name="TOutgoing">Type used by notifier to send data</typeparam>
        /// <typeparam name="TIncoming">Type sent by subscriber to send data back to notifier</typeparam>
        /// <param name="token">String identifying a message token</param>
        /// <param name="callback">Method to invoke when notified</param>
        protected void UnregisterToReceiveMessages<TOutgoing, TIncoming>(string token,
            EventHandler<NotificationEventArgs<TOutgoing, TIncoming>> callback)
        {
            // Unregister callback with MessageBusHelper and MessageBus
            this.messageBusHelper.Unregister(token, callback);
            MessageBus.Default.Unregister(token, this.messageBusHelper);
        }

        /// <summary>
        /// Notify registered subscribers.
        /// Call is transparently marshalled to UI thread.
        /// </summary>
        /// <param name="token">String identifying a message token</param>
        /// <param name="e">Event args carrying message</param>
        protected void SendMessage(string token, NotificationEventArgs e)
        {
            // Send notification through the MessageBus
            MessageBus.Default.Notify(token, this, e);
        }

        /// <summary>
        /// Notify registered subscribers.
        /// Call is transparently marshalled to UI thread.
        /// </summary>
        /// <typeparam name="TOutgoing">Type used by notifier to send data</typeparam>
        /// <param name="token">String identifying a message token</param>
        /// <param name="e">Event args carrying message</param>
        protected void SendMessage<TOutgoing>(string token,
            NotificationEventArgs<TOutgoing> e)
        {
            // Send notification through the MessageBus
            MessageBus.Default.Notify(token, this, e);
        }

        /// <summary>
        /// Notify registered subscribers.
        /// Call is transparently marshalled to UI thread.
        /// </summary>
        /// <typeparam name="TOutgoing">Type used by notifier to send data</typeparam>
        /// <typeparam name="TIncoming">Type sent by subscriber to send data back to notifier</typeparam>
        /// <param name="token">String identifying a message token</param>
        /// <param name="e">Event args carrying message</param>
        protected void SendMessage<TOutgoing, TIncoming>(string token,
            NotificationEventArgs<TOutgoing, TIncoming> e)
        {
            // Send notification through the MessageBus
            MessageBus.Default.Notify(token, this, e);
        }

        /// <summary>
        /// Notify registered subscribers asynchronously.
        /// Call is not marshalled to UI thread.
        /// </summary>
        /// <param name="token">String identifying a message token</param>
        /// <param name="e">Event args carrying message</param>
        protected void BeginSendMessage(string token, NotificationEventArgs e)
        {
            // Send notification through the MessageBus
            MessageBus.Default.BeginNotify(token, this, e);
        }

        /// <summary>
        /// Notify registered subscribers asynchronously.
        /// Call is not marshalled to UI thread.
        /// </summary>
        /// <typeparam name="TOutgoing">Type used by notifier to send data</typeparam>
        /// <param name="token">String identifying a message token</param>
        /// <param name="e">Event args carrying message</param>
        protected void BeginSendMessage<TOutgoing>(string token,
            NotificationEventArgs<TOutgoing> e)
        {
            // Send notification through the MessageBus
            MessageBus.Default.BeginNotify(token, this, e);
        }

        /// <summary>
        /// Notify registered subscribers asynchronously.
        /// Call is not marshalled to UI thread.
        /// </summary>
        /// <typeparam name="TOutgoing">Type used by notifier to send data</typeparam>
        /// <typeparam name="TIncoming">Type sent by subscriber to send data back to notifier</typeparam>
        /// <param name="token">String identifying a message token</param>
        /// <param name="e">Event args carrying message</param>
        protected void BeginSendMessage<TOutgoing, TIncoming>(string token,
            NotificationEventArgs<TOutgoing, TIncoming> e)
        {
            // Send notification through the MessageBus
            MessageBus.Default.BeginNotify(token, this, e);
        }

 
        #endregion
    }
}
