﻿using System;
using Telerik.Web.UI;

namespace Laxem.SharedControls.Telerik.AjaxUI.DatePicker
{
    public class LaxemDatePicker : RadDatePicker
    {
        public LaxemDatePicker()
        {
            SetProperties();
        }

        protected void SetProperties()
        {
            DateInput.DateFormat = "yyyy-MM-dd";
            SelectedDate = DateTime.Now;
            Skin = "Metro";
        }

    }
}
