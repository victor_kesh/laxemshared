﻿using Laxem.Portable.Interfaces;
using Laxem.Security.Extensions;
using System;

namespace Laxem.Security.Managers
{
    public abstract class BaseSecurityManager:ISecurityManager
    {

        #region properties
        public IUserSession UserSession { get;  set; }

        #endregion


        #region abstarct members


        #endregion

        #region methods 
        public bool AuthenticateApp(string appType, string salt, string hashCode)
        {
            return appType.GenerateHash(salt) == hashCode;
        }

        public string GenerateHash(string appType, string salt)
        {
            return appType.GenerateHash(salt);
        }

        public string GenerateUserHash(string password, string salt)
        {
            return password.GenerateHash(salt);
        }

        internal virtual string GenerateGUID()
        {
            Guid guid = Guid.NewGuid();
            return guid.ToString();
        }

        #endregion
    }
}
