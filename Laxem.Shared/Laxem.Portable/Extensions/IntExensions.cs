﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laxem.Portable.Extensions
{
   public static class IntExensions
    {
        public static string GetMonthDescriptionFromInt(this int monthInt)
        {
            string monthDescription = "";
            switch (monthInt)
            {
                case 1:
                    monthDescription = "JANUARY";
                    break;
                case 2:
                    monthDescription = "FEBRUARY";
                    break;
                case 3:
                    monthDescription = "MARCH";
                    break;
                case 4:
                    monthDescription = "APRIL";
                    break;
                case 5:
                    monthDescription = "MAY";
                    break;
                case 6:
                    monthDescription = "JUNE";
                    break;
                case 7:
                    monthDescription = "JULY";
                    break;
                case 8:
                    monthDescription = "AUGUST";
                    break;
                case 9:
                    monthDescription = "SEPTEMBER";
                    break;
                case 10:
                    monthDescription = "OCTOBER";
                    break;
                case 11:
                    monthDescription = "NOVEMBER";
                    break;
                case 12:
                    monthDescription = "DECEMBER";
                    break;
                default:
                    monthDescription = "INVALID";
                    break;

            }
            return monthDescription;
        }
    }

   
}
