﻿using System;

namespace Laxem.Portable.Interfaces
{
    public interface ICommonUserAccount
    {
        long ID { get; set; }
         Nullable<int> GroupId { get; set; }
        string FirstName { get; set; }
        string OtherNames { get; set; }
        string IDNo { get; set; }
        string TelNo { get; set; }
        string Mail { get; set; }
        string AccountPwd { get; set; }
        DateTimeOffset DateCreated { get; set; }
        bool AccountActivated { get; set; }
        string Active { get; set; }
    }
}
