﻿using System.Collections.Generic;
using System.Configuration;

namespace Laxem.Portable.BulkSms
{
    public static class BulkSmsUtility
    {
        public static string SendSms(string smsContent, string recipientTelNo)
        {
            var bulkSmsKey = ConfigurationManager.AppSettings["BulkSmsKey"];
            var bulkSmsUsername = ConfigurationManager.AppSettings["BulkSmsUsername"];
            var senderID = ConfigurationManager.AppSettings["BulkSmsSenderId"];

            if (bulkSmsKey != null && bulkSmsUsername != null && senderID != null)
            {
                var GateWay = new AfricasTalkingGateway(bulkSmsUsername, bulkSmsKey);
                var gatewayResponse = GateWay.sendMessage(recipientTelNo, smsContent, senderID);
                var sendStatus = ((Dictionary<string, object>)gatewayResponse[0])["status"].ToString();
                if (sendStatus == "Success")
                    return "Message sent";
                return "Send message failed";
            }
            else
            {
                return "Invalid sms configurations.";
            }

        }
    }
}
