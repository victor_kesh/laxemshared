﻿using Laxem.Portable.Interfaces;
using System;

namespace Laxem.Portable.Helpers
{
    public class OperationStatus : ICommonOperationStatus
    {
        private static OperationStatus _op = null;
        private static readonly object obj = new object();

        public string ErrorMessage { get; set; }

        public string Status { get; set; } = Helpers.Status.Fail;
        public object Result { get; set; }
        public Exception Exception { get; set; }
        private OperationStatus()
        {

        }
        public static OperationStatus CreateOpStatus()
        {
            ///lock applied to make  objectthread safe 
            lock (obj)
            {
                return _op == null ? new OperationStatus() : _op;
            }
        }
    }
}
