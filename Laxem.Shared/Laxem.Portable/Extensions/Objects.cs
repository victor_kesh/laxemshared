﻿using Newtonsoft.Json;

namespace Laxem.Portable.Extensions
{
    public static class Object
    {
        public static string JsonSerializeObject(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static object JsonDeSerializeObject(this string obj)
        {
            return JsonConvert.DeserializeObject(obj);
        }
    }
}
