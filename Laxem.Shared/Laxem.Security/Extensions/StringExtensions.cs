﻿
using Laxem.Security.Security;
using System;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace Laxem.Security.Extensions
{
    public static class StringExtensions
    {
        public static string DecryptCookieValue(this string cookieValue)
        {
            try
            {

                return Encryption.DecryptString(cookieValue);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string EncryptCookieValue(this string cookieValue)
        {
            try
            {
                return Encryption.EncryptString(cookieValue);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public static string GenerateHash(this string input, string salt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(input + salt);
            SHA256Managed sHA256ManagedString = new SHA256Managed();
            byte[] hash = sHA256ManagedString.ComputeHash(bytes);
            return Convert.ToBase64String(hash).Trim();
        }
    }
}
