﻿
using System;
using System.Net.Http;

namespace Laxem.Portable.Extensions
{
    public static class StringExtensions
    {
        public static StringContent ToJsonString(this string stringValue)
        {
            return new StringContent(stringValue, System.Text.Encoding.UTF8, "application/json");
        }
        public static string GetTimeStamp()
        {
            string month, day, hour, minute, millisecond;
            DateTime now = DateTime.Now;
            month = now.Month.ToString();
            day = now.Day.ToString();
            hour = now.Hour.ToString();
            minute = now.Minute.ToString();
            millisecond = now.Millisecond.ToString();

            if (month.Length == 1)
                month = $"0{month}";
            if (day.Length == 1)
                day = $"0{day}";
            if (hour.Length == 1)
                hour = $"0{hour}";
            if (minute.Length == 1)
                minute = $"0{minute}";
            if (millisecond.Length == 1)
                millisecond = $"0{millisecond}";
            millisecond = millisecond.Substring(0, 2);

            var timeStamp = now.Year.ToString().Substring(2, 2) + month + day
                + hour + minute + millisecond.Substring(0, 2);
            return timeStamp;
        }
    }
}
