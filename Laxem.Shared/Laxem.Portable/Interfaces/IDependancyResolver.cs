﻿using System;

namespace Laxem.Portable.Interfaces
{
    public interface IDependancyResolver
    {
         void RegisterType<T>(Func<object> createDelegate);
         T GetType<T>();
         bool IsTypeRegistered<T>();
    }
}
