﻿using Laxem.Portable.Interfaces;
using System;
using Unity;

namespace Laxem.Portable.DependencyResolution
{
    public class DependancyResolver 
    {
        private static  UnityContainer _container;
        public DependancyResolver(IUnityContainer container)
        {
            _container = (UnityContainer)container;
        }

        public static object GetType<T>()
        {
            return _container.Resolve<T>();
        }

        public static bool IsTypeRegistered<T>()
        {
            return _container.IsRegistered<T>();
        }

        public void RegisterType<T>(Func<object> createDelegate)
        {
            _container.RegisterType<T>();
        }
    }
}
