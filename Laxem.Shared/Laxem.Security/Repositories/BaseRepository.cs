﻿using Laxem.Portable.Helpers;
using Laxem.Portable.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using EntityState = System.Data.Entity.EntityState;

namespace Laxem.Security.Repositories
{
    public abstract class BaseRepository<T> : IGenericBaseRepository<T> where T : class
    {
        private DbContext _context;
        private OperationStatus _opState;
        public BaseRepository(DbContext context)
        {
            _context = context;
            _opState = OperationStatus.CreateOpStatus();
        }

        public virtual OperationStatus Add(T Entity)
        {
            try
            {
                _opState.Status = Status.Success;
                return _opState;
            }
            catch (Exception ex)
            {
                _opState.Status = Status.Fail;
                _opState.ErrorMessage = ex.Message;
                return _opState;
            }

        }

        public virtual OperationStatus AddAndGetId(T Entity)
        {
            try
            {
                long tempId = 0;
                var dbSet = _context.Set<T>();

                dbSet.Add(Entity);
                _context.SaveChanges();
                _opState.Status = Status.Success;
                _opState.Result = tempId;

                var _id = Entity.GetType().GetProperty("ID").GetValue(Entity, null);
                if (long.TryParse(_id.ToString(), out tempId))
                {
                    _opState.Status = Status.Success;
                    _opState.Result = _id;
                }

                return _opState;
            }
            catch (Exception ex)
            {
                _opState.Status = Status.Fail;
                _opState.ErrorMessage = ex.Message;
                _opState.Result = 0;
                return _opState;
            }



        }

        public virtual OperationStatus AddMany(List<T> items)
        {
            try
            {
                foreach (T item in items)
                {
                    _context.Entry(item).State = EntityState.Added;
                }
                _context.SaveChanges();
                _opState.Status = Status.Success;
                return _opState;
            }
            catch (Exception ex)
            {
                _opState.Status = Status.Fail;
                _opState.ErrorMessage = ex.Message;
                return _opState;
            }

        }

        public virtual List<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = _context.Set<T>();
            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);
            list = dbQuery
                 .AsNoTracking()
                 .Where(where)
                 .ToList<T>();
            return list;
        }

        public virtual OperationStatus Remove(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Deleted;
                _context.SaveChanges();
                _opState.Status = Status.Success;
                return _opState;
            }
            catch (Exception ex)
            {
                _opState.Status = Status.Fail;
                _opState.ErrorMessage = ex.Message;
                return _opState;
            }
        }

        public virtual OperationStatus Update(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();
                _opState.Status = Status.Success;
                return _opState;
            }
            catch (Exception ex)
            {
                _opState.Status = Status.Fail;
                _opState.ErrorMessage = ex.Message;
                return _opState;
            }
        }


    }
}
