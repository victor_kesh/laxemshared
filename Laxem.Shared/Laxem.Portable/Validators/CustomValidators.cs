﻿using System.Text.RegularExpressions;

namespace Laxem.Portable.Validators
{
    public static class CustomValidators
    {

        public static string ValidateKEPhone(string phoneNumber)
        {
            string validationMessage = string.Empty;
            if (string.IsNullOrEmpty(phoneNumber))
                validationMessage = "Please input phone number.";
            if (!phoneNumber.StartsWith("+254", System.StringComparison.OrdinalIgnoreCase))
                validationMessage = validationMessage + "\n" + "Phone number should be start with +254";
            return validationMessage;
        }
        internal static string EmailIsValid(string emailAddress)
        {
            var validRegex = CreateValidEmailRegex();
            bool isValid = validRegex.IsMatch(emailAddress);
            return isValid == true ? string.Empty : "Please input valid email";
        }
        private static Regex CreateValidEmailRegex()
        {
            string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
            return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
        }
    }
}
