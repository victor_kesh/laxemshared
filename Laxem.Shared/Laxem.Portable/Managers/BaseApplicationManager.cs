﻿using Laxem.Portable.Extensions;
using Laxem.Portable.Helpers;
using Laxem.Portable.Interfaces;
using Laxem.Portable.Utilities;
using Newtonsoft.Json;
using System;

namespace Laxem.Portable.Managers
{
    public abstract class BaseApplicationManager : IApplicationManager
    {
        public abstract int AllowedLoginTrials { get; }
        public abstract int ApplicationId { get; }
        public abstract string ApplicationVersion { get; }
        public abstract string ApplicationBaseUrl { get; }

        public abstract void SetSecurityManager(ISecurityManager securityManager);

        public ISecurityManager SecurityManager { get; protected set; }

        public string LoginHours
        {
            get
            {
                return SecurityManager.UserSession.LoginDateTime.GetSalutation();
            }
        }
        public virtual string HelloMessage
        {
            get
            {
                return $"{LoginHours} {SecurityManager.UserSession.CurrentUser}";
            }
        }
        protected internal RestUtility RestClient
        {
            get
            {
                return RestUtility.GetRestInstance();
            }
        }


        public virtual ICommonOperationStatus SendRestRequest(object obj, string address, RestSharp.Method method)
        {
            var op = OperationStatus.CreateOpStatus();
            try
            {
                address = $"{ApplicationBaseUrl}{address}";
                var response = RestClient.SendRequest(obj, address, method);
                var result = JsonConvert.DeserializeObject<ICommonOperationStatus>(response.Content);
                return result == null ? op : result;
            }
            catch (Exception ex)
            {
                op.ErrorMessage = ex.ToString();
                op.Result = null;
                op.Status = Status.Fail;
                return op;
            }
        }


    }
}
