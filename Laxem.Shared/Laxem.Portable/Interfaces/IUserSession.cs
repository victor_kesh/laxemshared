﻿using System;

namespace Laxem.Portable.Interfaces
{
    public interface IUserSession
    {
        ICommonUserAccount CurrentUser { get; set; }
        DateTimeOffset LoginDateTime { get; set; }
        bool IsSessionActive { get; set; }
        int CurrentUserLoginTrials { get; set; }
        int SessionTimeOutInMinutes { get; }
    }
}
