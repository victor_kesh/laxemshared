﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laxem.PubSub
{
    public interface INotifyable
    {
        void Notify(string token, object sender, NotificationEventArgs e);
    }
}
