﻿using RestSharp;

namespace Laxem.Portable.Utilities
{
    public class RestUtility
    {
        private static RestUtility _instance;
        private static readonly object obj = new object();
        public static RestUtility GetRestInstance()
        {
            lock (obj)
            {
                if (_instance == null)
                    _instance = new RestUtility();
                return _instance;
            }
        }

        public IRestResponse SendRequest(object obj, string address, Method method)
        {
            var request = new RestRequest(address, method);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(obj);
            var client = GetClient();
            return client.Execute(request);
        }

        private RestClient GetClient()
        {
            string username = "laxem";
            string password = "*##88!1HH";
            var client = new RestClient
            {
                Timeout = 3600,
                //Authenticator = new HttpBasicAuthenticator(username, password)
            };
            return client;
        }
    }
}
